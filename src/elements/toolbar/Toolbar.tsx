import React, { useEffect, useState } from "react";
import "./Toolbar.css";
import { texts } from "../../config/text";
import { Link, useMatch, useResolvedPath } from "react-router-dom";
import { LinkProps } from "react-router-dom";


const languages: string[] = ["EN", "RO", "RU"];
interface Page {
	name: string;
	path: string;
}
const pages: Page[] = [
	{ name: "home", path: "/" },
	{ name: "contacts", path: "/contacts" },
	{ name: "portfolio", path: "/portfolio" },
];


interface ToolbarProps {
	lang: string;
	setLang: any;
}

/**
 * Render app toolbar
 * @param {ToolbarProps} props contains language and set language
 * @return {JSX.Element} App root directory
 */
export default function Toolbar(props: ToolbarProps): JSX.Element {
	const { lang, setLang } = props;
	const [openMenu, setOpenMenu] = useState<boolean>(false);
	const [openLanguageSelect, setOpenLanguageSelect] = useState<boolean>(false);


	useEffect(() => {
		document.addEventListener("click", handleClick);
		
		/* return () => {
			document.removeEventListener("click", handleClick);
		}; */
	}, []);
	
	/**
	 * check what is clicked and close menus
	 * @param {MouseEvent} mouseEvent browser event object
	*/
	function handleClick(mouseEvent: MouseEvent) {
		// console.dir(e);
		// @ts-ignore
		if (mouseEvent?.target?.id !== "menuButton" && mouseEvent?.target?.id !== "menuIcon") {
			setOpenMenu(false);
		}
		
		// @ts-ignore
		if (mouseEvent?.target?.id !== "languageButton") {
			setOpenLanguageSelect(false);
		}
	}


	const onLanguageSelect = (language: string) => {
		setLang(language);
		setOpenLanguageSelect(false);
		
		localStorage.setItem("lang", language);
	};
	
	/**
	 * Render app toolbar
	 * @param {ToolbarProps} props contains language and set language
	 * @return {JSX.Element} App root directory
	*/
	function CustomLink({ children, to, ...props }: LinkProps) {
		const resolved = useResolvedPath(to);
		const match = useMatch({ path: resolved.pathname, end: true });

		return (
			<div>
				<Link
					className={ match ? "active" : "" }
					to={to}
					{...props}
				>
					{children}
				</Link>
			</div>
		);
	}
	
	
	return (
		<div className="topNav">
			
			<div className="toolbar">
				
				<div className={"leftToolsContainer"}>
					
					<div className="languageDropdown" id={"languageDropdown"}>
						
						<button
							id={"languageButton"}
							className="btn button toolBarButton"
							onClick={() => setOpenLanguageSelect(!openLanguageSelect)}
						>
							{
								lang
							}
						</button>
						
						{
							openLanguageSelect &&
								<div className="languageDropdownContent">
									{
										languages.map((language: string) => {
											return (
												<span
													key={language}
													onClick={() => onLanguageSelect(language)}
													className={lang === language ? "selected" : ""}
												>
													{
														language
													}
												</span>
											);
										})
									}
								</div>
						}
					
					</div>
					
				</div>
				
				<div className="navigationLinks">
					{
						pages.map((page: Page) => {
							return (
								<CustomLink
									to={page.path}
									key={page.name}
								>
									{texts.get(page.name)?.get(lang)}
								</CustomLink>
							);
						})
					}
				</div>
				
				<div className="menuDropdown">
					
					<button
						id={"menuButton"}
						className="btn button"
						onClick={() => setOpenMenu(!openMenu)}
					>
						<i className="material-icons" id={"menuIcon"}>
							menu
						</i>
					</button>
					
					{
						openMenu &&
							<div className="menuDropdownContent" onClick={() => setOpenMenu(false)}>
								{
									pages.map((page: Page) => {
										return (
											<CustomLink
												to={page.path}
												key={page.name}
											>
												{ texts.get(page.name)?.get(lang) }
											</CustomLink>
										);
									})
								}
							</div>
					}
				
				</div>
			
			</div>
		</div>
	);
}
