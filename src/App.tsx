import React, {useState} from "react";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import HomePage from "./pages/homepage/HomePage";
import ContactsPage from "./pages/contacts/ContactsPage";
import Toolbar from "./elements/toolbar/Toolbar";
import "./App.css";

/**
 * Render app root element
 * @return {JSX.Element} App root directory
 */
function App(): JSX.Element {
	const [lang, setLang] = useState<string>(localStorage.getItem("lang") || "EN");
	
	
	return (
		<div className="App">
			
			<Router>
				
				<Toolbar
					lang={lang}
					setLang={setLang}
				/>
				
				<div className="appContent">
					
					<Routes>
						<Route index element={<HomePage lang={lang}/>} />
						<Route path="/contacts" element={<ContactsPage lang={lang}/>} />
						<Route path="/portfolio" element={<ContactsPage lang={lang}/>} />
					</Routes>
				
				</div>
			
			</Router>
		
		</div>
	);
}

export default App;
