import React from "react";
import { texts } from "../../config/text";
import "./HomePage.css";
import isImage from "./is.jpg";


interface HomePageProps {
	lang: string;
}

/**
 * Render home page
 * @param {HomePageProps} props contains language
 * @return {JSX.Element} App root directory
 */
export default function HomePage(props: HomePageProps): JSX.Element {
	const { lang } = props;
	
	
	return (
		<div className="page">
			
			<div id={"homePageContent"}>
				
				<header id={"homePageHeader"}>
					
					<div className={"headerBlock"}>
						<img className={"headerPhoto"} id={"is"} src={isImage} alt={""}/>
					</div>
					
					<div className={"headerBlock"}>
						<h3>
							{
								texts.get("greeting")?.get(lang)
							}
						</h3>
						
						
					</div>
					
				</header>
				
				<div id={"homePageBody"}>
				
					<div className={"time left"}>
						<span>
							Dec 2021
						</span>
					</div>

					<div className="block rightBlock midle first">
						<h3>
							{
								texts.get("frontendLabel")?.get(lang)
							}
						</h3>
						
						<p>
							{
								texts.get("frontendBody")?.get(lang)
							}
						</p>
					</div>


					<div className={"time left"}>
						<span>
							Jan 2018
						</span>
					</div>
					<div className="block rightBlock midle">
						<h3>
							{
								texts.get("fullstackLabel")?.get(lang)
							}
						</h3>
						
						<p>
							{
								texts.get("fullstackBody")?.get(lang)
							}
						</p>
					</div>


					<div className={"left"}>
					</div>
					<div className={"midHorizBlock  midle"}
					>
						<div id="topLineBlock"/>
						<div id="bottomLineBlock"/>
					</div>
					<div className={"right"}>
					</div>


					<div className={"block leftBlock"} >
						<h3>
							{
								texts.get("supplyChainAnalystLabel")?.get(lang)
							}
						</h3>
						
						<p>
							{
								texts.get("supplyChainAnalystBody")?.get(lang)
							}
						</p>
					</div>
					<div className="time right">
						<span>
							Mar 2017
						</span>
					</div>


					<div className={"block leftBlock"} >
						<h3>
							{
								texts.get("androidDeveloperLabel")?.get(lang)
							}
						</h3>
						
						<p>
							{
								texts.get("androidDeveloperBody")?.get(lang)
							}
						</p>
					</div>
					<div className="time right">
						<span>
							Feb 2015
						</span>
					</div>


					<div className={"block leftBlock"} >
						<h3>
							{
								texts.get("logisticsLabel")?.get(lang)
							}
						</h3>
						
						<p>
							{
								texts.get("logisticsBody")?.get(lang)
							}
						</p>
					</div>

					<div className="time right">
						<span>
							Jun 2010
						</span>
					</div>


					<div className={"block leftBlock"} >
						<h3>
							{
								texts.get("callCenterRepresentativeLabel")?.get(lang)
							}
						</h3>
						
						<p>
							{
								texts.get("callCenterRepresentativeBody")?.get(lang)
							}
						</p>
					</div>

					<div className="time right">
						<span>
							Sep 2007
						</span>
					</div>

				
				</div>
				
			</div>

		</div>
	);
}
