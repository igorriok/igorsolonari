import React from "react";
import {texts} from "../../config/text";
import "./ContactsPage.css";


interface HomePageProps {
	lang: string;
}

/**
 * Adds two numbers together.
 * @param {HomePageProps} props contains language
 * @return {JSX.Element} The sum of the two numbers.
 */
export default function ContactsPage(props: HomePageProps): JSX.Element {
	const {lang} = props;
	
	
	return (
		<div className="page">
			
			<div className={"contactContainer"}>
				
				<h4 className={"contactsTitle"}>
					{
						texts.get("contacts")?.get(lang) + ":"
					}
				</h4>
				
				<div className={"contactInfo"}>
					
					<table>
						<tbody>
							<tr>
								<td>
									{
										texts.get("phone")?.get(lang) + ":"
									}
								</td>
								<td>
									<a href="tel:+37362078526">+373-62078526</a>
								</td>
							</tr>
						</tbody>
					</table>
					
					<p>
						Email:&nbsp;
						<a href={"mailto:igor@solonari.net"}>
							igor@solonari.net
						</a>
					</p>
					
					
				</div>
			</div>
		</div>
	);
}
